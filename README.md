# blocks_game

Simple "block building" game written for educational purposes
using Rust and [eframe](https://docs.rs/eframe/latest/eframe/)
GUI library.

## Features

- support for classic block types
- brick collision detection
- game timer
- game pause
- indication of next block
- points counting (1 point per reduced row, 100 - if all bricks of the same color)
- non-linear block speed increase over time
