use eframe::{
    egui::{self, Painter},
    epaint::{Color32, Pos2, Rect, Rounding, Shape, Stroke, Vec2},
};

pub struct Brick {
    id: u64,
    color: Color32,
    pos: Pos2,
    future_pos: Pos2,
}

impl Brick {
    pub const SIZE: f32 = 18.0;
    pub const SIZE_VEC: Vec2 = Vec2::new(Self::SIZE, Self::SIZE);

    fn new(id: u64, pos: Pos2, color: Color32) -> Self {
        Self {
            id,
            pos,
            future_pos: Pos2::ZERO,
            color,
        }
    }

    pub fn future_pos(&self) -> Pos2 {
        self.future_pos
    }

    pub fn future_rect(&self) -> Rect {
        Rect::from_min_size(self.future_pos, Self::SIZE_VEC)
    }

    pub fn set_future_pos_by_vec(&mut self, pos_modifier: Vec2) {
        self.future_pos = self.pos + pos_modifier;
    }

    pub fn apply_vec_to_future_pos(&mut self, pos_modifier: Vec2) {
        self.future_pos += pos_modifier;
    }

    pub fn set_future_pos_rotate(&mut self, origin: Pos2, size_in_bricks: Vec2) {
        let rel_pos = self.get_rel_pos(origin);
        let new_x = size_in_bricks.y - 1.0 - rel_pos.y;
        let rot_rel_pos = Vec2::new(new_x, rel_pos.x);
        self.future_pos = self.get_abs_pos(origin, rot_rel_pos);
    }

    pub fn commit_future_pos(&mut self) {
        self.pos = self.future_pos;
    }

    pub fn rect(&self) -> Rect {
        Rect::from_min_size(self.pos, Self::SIZE_VEC)
    }

    pub fn pos(&self) -> Pos2 {
        self.pos
    }

    fn get_rel_pos(&self, origin: Pos2) -> Vec2 {
        (self.pos.to_vec2() - origin.to_vec2()) / Self::SIZE_VEC
    }

    fn get_abs_pos(&self, origin: Pos2, rel_pos: Vec2) -> Pos2 {
        origin + rel_pos * Self::SIZE_VEC
    }

    pub fn get_animated_pos(&self, ui: &mut egui::Ui) -> Pos2 {
        let brick_id = ui.layer_id().id.with(self.id);
        let ctx = ui.ctx();
        let animated_x = ctx.animate_value_with_time(brick_id.with(1), self.pos.x, 0.1);
        let animated_y = ctx.animate_value_with_time(brick_id.with(2), self.pos.y, 0.1);
        Pos2::new(animated_x, animated_y)
    }

    pub fn paint(&self, ui: &mut egui::Ui, painter: &Painter) {
        let animated_pos = self.get_animated_pos(ui);

        let margin_vec = Vec2::new(1.0, 1.0) * 2.0;
        let rect =
            Rect::from_min_size(animated_pos + margin_vec, Self::SIZE_VEC - margin_vec * 2.0);
        let stroke_color = self.color.linear_multiply(0.2);
        let rounding = Rounding::same(2.0);

        let shapes = vec![
            Shape::rect_filled(rect.shrink(2.0), rounding, self.color),
            Shape::rect_stroke(rect, rounding, Stroke::new(margin_vec.x, stroke_color)),
        ];
        painter.extend(shapes);
    }

    pub fn all_have_same_color(bricks: &Vec<Brick>) -> bool {
        if !bricks.is_empty() {
            let mut iter = bricks.iter();
            let first_color = iter.next().unwrap().color;
            for brick in iter {
                if brick.color != first_color {
                    return false;
                }
            }
            return true;
        }

        false
    }
}

#[derive(Default)]
pub struct BrickFactory {
    next_id: u64,
}

impl BrickFactory {
    pub fn create(&mut self, pos: Pos2, color: Color32) -> Brick {
        let brick = Brick::new(self.next_id, pos, color);
        self.next_id += 1;
        brick
    }
}
