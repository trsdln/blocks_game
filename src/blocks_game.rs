use eframe::{egui, epaint::Vec2};

use crate::{brick::Brick, display::Display};

#[derive(Default)]
pub struct BlocksGame {
    display: Display,
}

impl eframe::App for BlocksGame {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        let repaint_duration = std::time::Duration::from_millis(10);
        ctx.request_repaint_after(repaint_duration);

        egui::CentralPanel::default().show(ctx, |ui| {
            self.display.update(ui);
        });
    }
}

impl BlocksGame {
    pub fn get_desired_window_size() -> Vec2 {
        Display::SIZE_IN_BRICKS * Brick::SIZE
            + Vec2::new(Display::INFO_AREA_WIDTH_IN_BRICKS * Brick::SIZE, 0.0)
            + Vec2::new(20.0, 20.0)
    }
}
