use eframe::epaint::Vec2;

use crate::brick::Brick;

#[derive(PartialEq)]
pub enum MoveDirection {
    Right,
    Left,
    Down,
    Rotate,
}

impl MoveDirection {
    pub fn get_vec(&self) -> Option<Vec2> {
        let unit = Brick::SIZE;
        match self {
            Self::Right => Some(Vec2::new(unit, 0.0)),
            Self::Left => Some(Vec2::new(-unit, 0.0)),
            Self::Down => Some(Vec2::new(0.0, unit)),
            Self::Rotate => None,
        }
    }
}
