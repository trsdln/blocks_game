type TimeValue = f64;

pub struct Timer {
    last_measure_at: TimeValue,
    total_time: TimeValue,
    is_started: bool,
}

impl Timer {
    pub fn new() -> Self {
        Timer {
            last_measure_at: 0.0,
            total_time: 0.0,
            is_started: false,
        }
    }

    pub fn toggle(&mut self) {
        self.is_started = !self.is_started;
    }

    pub fn reset(&mut self) {
        self.is_started = false;
        self.total_time = 0.0;
    }

    pub fn is_on(&self) -> bool {
        self.is_started
    }

    /// Note: ensure it is called before `.toggle`,
    /// so current time is initialized
    /// before measuring is started
    pub fn track_time(&mut self, current_time: TimeValue) {
        if self.is_started {
            self.total_time += current_time - self.last_measure_at;
        }

        self.last_measure_at = current_time;
    }

    pub fn get_total_time(&self) -> TimeValue {
        self.total_time
    }
}
