use eframe::{
    egui::{self, Key, Painter},
    epaint::{Color32, FontId, Fonts, Pos2, Rect, Rounding, Shape, Stroke, Vec2},
};

use crate::{
    block_pattern::{BlockPattern, RandomPatternGenerator},
    brick::{Brick, BrickFactory},
    brick_group::BrickGroup,
    move_direction::MoveDirection,
    timer::Timer,
};

pub struct Display {
    size: Vec2,
    last_recalc_time: f64,
    block: BrickGroup,
    next_pattern: BlockPattern,
    next_block: BrickGroup,
    wall: BrickGroup,
    pattern_generator: RandomPatternGenerator,
    brick_factory: BrickFactory,
    show_debug_info: bool,
    is_game_over: bool,
    points: i32,
    /// Introduces short delay between a block touching
    /// the wall and it becoming the wall's part
    wall_touch_timer: Timer,
    game_timer: Timer,
}

impl Default for Display {
    fn default() -> Self {
        let mut pattern_generator = RandomPatternGenerator::default();

        let mut game_timer = Timer::new();
        game_timer.toggle();

        Display {
            size: Self::SIZE_IN_BRICKS * Brick::SIZE,
            last_recalc_time: 0.0,
            points: 0,
            wall_touch_timer: Timer::new(),
            game_timer,
            block: BrickGroup::default(),
            next_pattern: pattern_generator.next(),
            next_block: BrickGroup::new(vec![]),
            wall: BrickGroup::new(vec![]),
            pattern_generator,
            brick_factory: BrickFactory::default(),
            show_debug_info: true,
            is_game_over: false,
        }
    }
}

impl Display {
    pub const SIZE_IN_BRICKS: Vec2 = Vec2::new(12.0, 35.0);
    pub const INFO_AREA_WIDTH_IN_BRICKS: f32 = 4.0;

    fn get_control_vectors(&mut self, ctx: &egui::Context) -> Vec<MoveDirection> {
        ctx.input(|i| {
            let mut res = vec![];

            if i.key_pressed(Key::Space) && !self.is_game_over {
                self.game_timer.toggle();
            }

            if i.key_pressed(Key::Backspace) {
                self.show_debug_info = !self.show_debug_info;
            }

            if !self.game_timer.is_on() || self.is_game_over {
                return res;
            }

            if i.key_pressed(Key::ArrowRight) {
                res.push(MoveDirection::Right);
            }

            if i.key_pressed(Key::ArrowLeft) {
                res.push(MoveDirection::Left);
            }

            if i.key_pressed(Key::ArrowDown) {
                res.push(MoveDirection::Down);
            }

            if i.key_pressed(Key::ArrowUp) {
                res.push(MoveDirection::Rotate)
            }

            res
        })
    }

    fn put_new_block_when_needed(&mut self, zero_pos: Pos2) {
        if self.block.is_empty() {
            let pattern_size = self.next_pattern.get_size();
            let initial_pos = zero_pos
                + Vec2::new(
                    ((Self::SIZE_IN_BRICKS.x - pattern_size.x) / 2.0).floor(),
                    -pattern_size.y,
                ) * Brick::SIZE;

            let bricks = self
                .next_pattern
                .get_bricks(initial_pos, &mut self.brick_factory);
            self.block.replace_bricks(bricks);

            self.next_pattern = self.pattern_generator.next();
            let next_initial_pos = zero_pos
                + Vec2::new(Self::SIZE_IN_BRICKS.x + 1.0, Self::SIZE_IN_BRICKS.y / 2.0)
                    * Brick::SIZE;
            let next_bricks = self
                .next_pattern
                .get_bricks(next_initial_pos, &mut self.brick_factory);
            self.next_block.replace_bricks(next_bricks);
        }
    }

    pub fn update(&mut self, ui: &mut egui::Ui) {
        let painter = Painter::new(
            ui.ctx().clone(),
            ui.layer_id(),
            ui.available_rect_before_wrap(),
        );

        // get drawable Rect of the window
        let painter_rect = painter.clip_rect();
        // add small margin to compensate 2px stroke
        let zero_pos = painter_rect.left_top() + Vec2::new(1.0, 1.0);

        let ctx = ui.ctx().clone();
        let orig_time = ctx.input(|i| i.time);
        self.game_timer.track_time(orig_time);
        let speed_modifier = 1.0 + (self.game_timer.get_total_time() + 2.0).ln() * 1.3;

        let display_rect = Rect::from_min_size(zero_pos, self.size);

        self.put_new_block_when_needed(zero_pos);

        let mut control_moves = self.get_control_vectors(&ctx);

        if self.game_timer.is_on() && !self.is_game_over {
            let time = (orig_time * speed_modifier).floor();
            if time > self.last_recalc_time {
                control_moves.push(MoveDirection::Down);
                self.last_recalc_time = time;
            }

            self.wall_touch_timer.track_time(orig_time);
        }

        let block_cage = self.get_block_cage(&display_rect);

        for movement in control_moves.iter() {
            if !self
                .block
                .move_if_possible(movement, &self.wall, &block_cage)
            {
                if self.wall_touch_timer.is_on() {
                    if self.wall_touch_timer.get_total_time() > 0.5 {
                        self.block.transport_bricks(&mut self.wall);
                        self.wall_touch_timer.reset();
                    }
                } else {
                    self.wall_touch_timer.toggle();
                }
            }
        }

        self.points += self.wall.remove_filled_rows(Self::SIZE_IN_BRICKS.x as i32);

        if self.wall.any_brick_outside_rect(&display_rect) {
            self.is_game_over = true;
            if self.game_timer.is_on() {
                self.game_timer.toggle();
            }
        }

        let shape =
            Shape::rect_stroke(display_rect, Rounding::ZERO, Stroke::new(2.0, Color32::RED));
        painter.add(shape);

        self.block.paint(ui, &painter);
        self.next_block.paint(ui, &painter);
        self.wall.paint(ui, &painter);

        if let Some((left_pos, right_pos)) = self.block.get_guidelines_pos(ui) {
            let guideline_stroke = Stroke::new(1.0, Color32::RED.linear_multiply(0.1));
            let guidelines = vec![
                Shape::line_segment(
                    [left_pos, Pos2::new(left_pos.x, display_rect.bottom())],
                    guideline_stroke,
                ),
                Shape::line_segment(
                    [right_pos, Pos2::new(right_pos.x, display_rect.bottom())],
                    guideline_stroke,
                ),
            ];
            painter.extend(guidelines);
        }

        let text_shapes = ctx.fonts(|fonts| {
            let play_time = self.game_timer.get_total_time() as u64;
            let mut shapes = vec![Shape::text(
                fonts,
                display_rect.right_top() + Vec2::new(0.5, 2.0) * Brick::SIZE,
                egui::Align2::LEFT_TOP,
                format!(
                    "{:05}\n{:02}:{:02}",
                    self.points,
                    (play_time / 60),
                    play_time % 60
                ),
                FontId::monospace(20.0),
                Color32::GREEN,
            )];

            if self.is_game_over {
                shapes.push(self.create_status_text(
                    "Game Over",
                    Color32::RED,
                    &display_rect,
                    fonts,
                ));
            }

            if !self.game_timer.is_on() && !self.is_game_over {
                shapes.push(self.create_status_text("Pause", Color32::GOLD, &display_rect, fonts));
            }

            shapes
        });
        painter.extend(text_shapes);

        if self.show_debug_info {
            let fps_count = (1.0 / ui.input(|i| i.stable_dt)).floor();
            // highly unlikely we will get more than 60 actual FPS:
            let fps_count = if fps_count > 60.0 { 60.0 } else { fps_count };

            let debug_info = vec![
                format!("FPS: {}", fps_count),
                format!("S: {}", speed_modifier.floor()),
            ];

            let debug_info = debug_info.join("\n");

            painter.debug_text(
                display_rect.right_top() + Vec2::new(0.5, 0.0) * Brick::SIZE,
                egui::Align2::LEFT_TOP,
                Color32::GRAY,
                debug_info,
            );
        }
    }

    fn create_status_text(
        &self,
        text: &str,
        color: Color32,
        display_rect: &Rect,
        fonts: &Fonts,
    ) -> Shape {
        Shape::text(
            fonts,
            display_rect.center(),
            egui::Align2::CENTER_CENTER,
            text,
            FontId::monospace(36.0),
            color.linear_multiply(0.6),
        )
    }

    fn get_block_cage(&self, disp_rect: &Rect) -> Rect {
        let mut block_cage = *disp_rect;
        // allow blocks to move above display Rect:
        block_cage.set_top(block_cage.top() - Brick::SIZE * 5.0);
        block_cage
    }
}
