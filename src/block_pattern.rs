use eframe::epaint::{Color32, Pos2, Vec2};
use rand::{rngs::StdRng, RngCore, SeedableRng};

use crate::brick::{Brick, BrickFactory};

#[derive(Clone, Copy)]
pub enum BlockPatternType {
    Square,
    Stick,
    LeftTwistSquare,
    RightTwistSquare,
    RightL,
    LeftL,
    Spike,
}

impl BlockPatternType {
    pub fn get_brick_locations(&self) -> Vec<Vec2> {
        let tuples = match &self {
            BlockPatternType::Square => vec![(0, 0), (0, 1), (1, 0), (1, 1)],
            BlockPatternType::Stick => vec![(0, 0), (0, 1), (0, 2), (0, 3)],
            BlockPatternType::LeftTwistSquare => {
                vec![(1, 0), (0, 1), (1, 1), (0, 2)]
            }
            BlockPatternType::RightTwistSquare => {
                vec![(0, 0), (0, 1), (1, 1), (1, 2)]
            }
            BlockPatternType::RightL => {
                vec![(0, 0), (0, 1), (0, 2), (1, 2)]
            }
            BlockPatternType::LeftL => {
                vec![(1, 0), (1, 1), (1, 2), (0, 2)]
            }
            BlockPatternType::Spike => {
                vec![(0, 0), (0, 1), (1, 1), (0, 2)]
            }
        };

        tuples_to_vectors(tuples)
    }
}

pub struct BlockPattern {
    pattern_type: BlockPatternType,
    color: Color32,
}

impl BlockPattern {
    pub fn get_bricks(&self, left_top_pos: Pos2, brick_factory: &mut BrickFactory) -> Vec<Brick> {
        self.pattern_type
            .get_brick_locations()
            .iter()
            .map(|brick_vec| {
                brick_factory.create(left_top_pos + *brick_vec * Brick::SIZE, self.color)
            })
            .collect()
    }

    pub fn get_size(&self) -> Vec2 {
        let max = |a: f32, b: f32| if a > b { a } else { b };
        let (max_x, max_y) = self
            .pattern_type
            .get_brick_locations()
            .iter()
            .fold((0.0, 0.0), |(x, y), vec| (max(x, vec.x), max(y, vec.y)));
        Vec2::new(max_x + 1.0, max_y + 1.0)
    }
}

pub struct RandomPatternGenerator {
    rng: StdRng,
    types: Vec<BlockPatternType>,
    colors: Vec<Color32>,
}

impl Default for RandomPatternGenerator {
    fn default() -> Self {
        Self {
            rng: StdRng::from_entropy(),
            types: vec![
                BlockPatternType::Square,
                BlockPatternType::Stick,
                BlockPatternType::LeftTwistSquare,
                BlockPatternType::RightTwistSquare,
                BlockPatternType::RightL,
                BlockPatternType::LeftL,
                BlockPatternType::Spike,
            ],
            colors: vec![
                Color32::GREEN,
                Color32::GRAY,
                Color32::GOLD,
                Color32::BLUE,
                Color32::RED,
            ],
        }
    }
}

impl RandomPatternGenerator {
    fn get_random_idx(&mut self, max: usize) -> usize {
        (self.rng.next_u32() % max as u32) as usize
    }

    pub fn next(&mut self) -> BlockPattern {
        let type_idx = self.get_random_idx(self.types.len());
        let pattern_type = *self.types.get(type_idx).unwrap();

        let color_idx = self.get_random_idx(self.colors.len());
        let color = *self.colors.get(color_idx).unwrap();

        BlockPattern {
            pattern_type,
            color,
        }
    }
}

fn tuples_to_vectors(tuples: Vec<(usize, usize)>) -> Vec<Vec2> {
    tuples
        .into_iter()
        .map(|(x, y)| Vec2::new(x as f32, y as f32))
        .collect()
}
