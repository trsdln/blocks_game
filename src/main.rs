#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]
// hide console window on Windows in release

use blocks_game::BlocksGame;

fn main() -> Result<(), eframe::Error> {
    let window_size = BlocksGame::get_desired_window_size();
    let options = eframe::NativeOptions {
        initial_window_size: Some(window_size),
        resizable: false,
        icon_data: Some(
            eframe::IconData::try_from_png_bytes(&include_bytes!("../icon.png")[..]).unwrap(),
        ),
        ..Default::default()
    };
    eframe::run_native(
        "Blocks Game",
        options,
        Box::new(|_| Box::<BlocksGame>::default()),
    )
}
