use std::collections::HashMap;

use eframe::{
    egui,
    epaint::{Pos2, Rect, Vec2},
};

use crate::{brick::Brick, move_direction::MoveDirection};

#[derive(Default)]
pub struct BrickGroup {
    bricks: Vec<Brick>,
}

impl BrickGroup {
    pub fn new(bricks: Vec<Brick>) -> Self {
        Self { bricks }
    }

    pub fn replace_bricks(&mut self, bricks: Vec<Brick>) {
        self.bricks = bricks;
    }

    pub fn is_empty(&self) -> bool {
        self.bricks.is_empty()
    }

    pub fn transport_bricks(&mut self, dest_group: &mut Self) {
        dest_group.bricks.append(&mut self.bricks);
    }

    fn get_bricks_union_rect(&self) -> Option<Rect> {
        self.bricks.iter().fold(None, |opt_rect, brick| {
            let brick_rect = brick.rect();
            let new_rect = opt_rect.map_or(brick_rect, |acc_brick| acc_brick.union(brick_rect));
            Some(new_rect)
        })
    }

    fn is_future_bricks_pos_free(&self, brick_wall: &Self, disp_rect: &Rect) -> bool {
        for brick in self.bricks.iter() {
            // check if is outside of display
            if !disp_rect.contains_rect(brick.future_rect()) {
                return false;
            }

            // check for collisions with brick wall
            for wall_brick in brick_wall.bricks.iter() {
                if wall_brick.pos() == brick.future_pos() {
                    return false;
                }
            }
        }

        true
    }

    /// Returns `false` if rotation is impossible
    fn set_bricks_rotation_pos(&mut self, brick_wall: &Self, disp_rect: &Rect) -> bool {
        if let Some(block_rect) = self.get_bricks_union_rect() {
            let size_in_bricks = block_rect.size() / Brick::SIZE;
            let origin = block_rect.left_top();

            for brick in self.bricks.iter_mut() {
                brick.set_future_pos_rotate(origin, size_in_bricks);
            }

            if self.is_future_bricks_pos_free(brick_wall, disp_rect) {
                return true;
            }

            // allow block to rotate when there is free space on the left
            let size_diff = (size_in_bricks.y - size_in_bricks.x) as i32;
            if size_diff > 0 {
                if let Some(left_move) = MoveDirection::Left.get_vec() {
                    for _ in 0..size_diff {
                        for brick in self.bricks.iter_mut() {
                            brick.apply_vec_to_future_pos(left_move);
                        }

                        if self.is_future_bricks_pos_free(brick_wall, disp_rect) {
                            return true;
                        }
                    }
                }
            }
        }

        false
    }

    /// Returns `false` if block cannot move anymore
    pub fn move_if_possible(
        &mut self,
        movement: &MoveDirection,
        brick_wall: &Self,
        disp_rect: &Rect,
    ) -> bool {
        if let Some(move_vec) = movement.get_vec() {
            for brick in self.bricks.iter_mut() {
                brick.set_future_pos_by_vec(move_vec);
            }

            if !self.is_future_bricks_pos_free(brick_wall, disp_rect) {
                return *movement != MoveDirection::Down;
            }
        } else if *movement == MoveDirection::Rotate
            && !self.set_bricks_rotation_pos(brick_wall, disp_rect)
        {
            return true;
        }

        for brick in self.bricks.iter_mut() {
            brick.commit_future_pos();
        }

        true
    }

    pub fn remove_filled_rows(&mut self, row_width: i32) -> i32 {
        // count bricks at every row
        let mut bricks_per_row = HashMap::new();
        for brick in self.bricks.iter() {
            let key = brick.pos().y as i32;
            if let Some(count) = bricks_per_row.get_mut(&key) {
                *count += 1;
            } else {
                bricks_per_row.insert(key, 1);
            }
        }

        let mut points = 0;

        // Note: we want to remove rows from top to the bottom
        // in order to prevent other filled rows from shifting.
        // At the same time HashMap's keys are provided in arbitrary order.
        // Also, from game's perspective it doesn't matter whether
        // row is removed at current frame or next.
        let mut min_removed_key = 0;
        for (key, count) in bricks_per_row.iter() {
            let y = *key as f32;
            if *count == row_width && *key > min_removed_key {
                min_removed_key = *key;
                // here's how you use filter in Rust
                // to remove Vec's elements (safer than .remove() method):
                let mut temp_bricks = vec![];
                std::mem::swap(&mut temp_bricks, &mut self.bricks);
                let (bricks_to_keep, bricks_to_remove): (Vec<_>, Vec<_>) = temp_bricks
                    .into_iter()
                    .partition(|brick| brick.pos().y != y);
                self.bricks = bricks_to_keep;

                // move bricks above 1 row down
                if let Some(down_vec) = MoveDirection::Down.get_vec() {
                    for brick in self.bricks.iter_mut() {
                        if brick.pos().y < y {
                            brick.set_future_pos_by_vec(down_vec);
                            brick.commit_future_pos();
                        }
                    }
                }

                if Brick::all_have_same_color(&bricks_to_remove) {
                    points += 100;
                } else {
                    points += 1;
                }
            }
        }

        points
    }

    pub fn get_guidelines_pos(&self, ui: &mut egui::Ui) -> Option<(Pos2, Pos2)> {
        let brick_positions: Vec<Pos2> =
            self.bricks.iter().map(|b| b.get_animated_pos(ui)).collect();

        let compare_pos_x = |p1: &&Pos2, p2: &&Pos2| (p1.x as i32).cmp(&(p2.x as i32));
        let get_pos_x = |p: &Pos2| p.x;

        let left_x = brick_positions.iter().min_by(compare_pos_x).map(get_pos_x);
        let right_x = brick_positions.iter().max_by(compare_pos_x).map(get_pos_x);

        left_x
            .zip(right_x)
            .and_then(|(left_x, right_x)| {
                let compare_pos_y = |p1: &&Pos2, p2: &&Pos2| (p1.y as i32).cmp(&(p2.y as i32));
                let get_pos_y = |p: &Pos2| p.y;

                let left_y = brick_positions
                    .iter()
                    .filter(|p| p.x == left_x)
                    .max_by(compare_pos_y)
                    .map(get_pos_y);

                let right_y = brick_positions
                    .iter()
                    .filter(|p| p.x == right_x)
                    .max_by(compare_pos_y)
                    .map(get_pos_y);

                Some((left_x, right_x)).zip(left_y.zip(right_y))
            })
            .map(|((left_x, right_x), (left_y, right_y))| {
                (
                    Pos2::new(left_x, left_y) + Vec2::new(0.0, Brick::SIZE),
                    Pos2::new(right_x, right_y) + Vec2::new(Brick::SIZE, Brick::SIZE),
                )
            })
    }

    pub fn any_brick_outside_rect(&self, rect: &Rect) -> bool {
        self.bricks
            .iter()
            .any(|brick| !rect.contains_rect(brick.rect()))
    }

    pub fn paint(&self, ui: &mut egui::Ui, painter: &egui::Painter) {
        for brick in self.bricks.iter() {
            brick.paint(ui, painter);
        }
    }
}
